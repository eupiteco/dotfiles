function ResetBackground(color)
	color = color or "everforest"

	vim.api.nvim_set_var("everforest_background", "soft")
	vim.api.nvim_set_var("everforest_ui_contrast", "low")

	vim.cmd.colorscheme(color)
	-- vim.api.nvim_set_hl(0, "Normal", { bg = "none" })
	-- vim.api.nvim_set_hl(0, "NormalFloat", { bg = "none" })
end

ResetBackground()
