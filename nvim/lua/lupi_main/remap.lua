vim.g.mapleader = " "
vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)

vim.keymap.set("n", "j", "h")
vim.keymap.set("n", "k", "j")
vim.keymap.set("n", "l", "k")
vim.keymap.set("n", ";", "l")
vim.keymap.set("n", "h", ";")

vim.keymap.set("n", "<Leader>K", "<C-W>J")
vim.keymap.set("n", "<Leader>L", "<C-W>K")
vim.keymap.set("n", "<Leader>J", "<C-W>H")
vim.keymap.set("n", "<Leader>:", "<C-W>L")

vim.keymap.set("n", "<Leader>k", "<C-W><C-J>")
vim.keymap.set("n", "<Leader>l", "<C-W><C-K>")
vim.keymap.set("n", "<Leader>j", "<C-W><C-H>")
vim.keymap.set("n", "<Leader>;", "<C-W><C-L>")

vim.keymap.set("i", "{<CR>", "{<CR>}<Esc>")
vim.keymap.set("i", "kj", "<ESC>")
vim.keymap.set("v", "kj", "<ESC>")

vim.keymap.set("v", "<C-c", "\"+y")

vim.keymap.set("v", "<leader>/", "yq/p<Enter>")

vim.keymap.set("n", "<leader>cn", "*Ncgn")

vim.keymap.set("n", "<leader>q", ":q!<Enter>")
vim.keymap.set("n", "<leader>Q", ":wqa!<Enter>")
vim.keymap.set("n", "<leader>w", ":wa<Enter>")
vim.keymap.set("n", "<leader>t", ":w<Enter>:b#<Enter>")

-- vim.keymap.set("i", "(<CR>", "(<CR>)<Esc>")
-- vim.keymap.set("i", "[<CR>", "[<CR>]<Esc>")
-- vim.keymap.set("i", "{<Space>", "{}<Esc>i")
-- vim.keymap.set("i", "(<Space>", "()<Esc>i")
-- vim.keymap.set("i", "[<Space>", "[]<Esc>i")
-- vim.keymap.set("i", "<<Space>", "<><Esc>i")
-- vim.keymap.set("i", "\"<Space>", "\"\"<Esc>i")
-- vim.keymap.set("i", "'<Space>", "''<Esc>i")

vim.keymap.set("v", "{}", "c{}<Esc>P")
vim.keymap.set("v", "[]", "c[]<Esc>P")
vim.keymap.set("v", "()", "c()<Esc>P")
vim.keymap.set("v", "\"\"", "c\"\"<Esc>P")
vim.keymap.set("v", "''", "c''<Esc>P")

vim.keymap.set("n", "<leader>zf", ":set foldlevel=3")

vim.keymap.set("v", "K", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "L", ":m '<-2<CR>gv=gv")

vim.keymap.set("n", "J", "mzJ`z")
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

vim.keymap.set("x", "<leader>p", "\"_dP")
