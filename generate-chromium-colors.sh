#!/bin/bash

THEME_NAME="gruvbox-dark"
TMP_COLOR="/tmp/colors.sh"
THEME_DIR="/tmp/ch-theme"

cat "colors/$THEME_NAME.xresources" | grep \# | grep -v ! |cut -c 2- | sed 's/: \+/=/' > "$TMP_COLOR"
chmod +x $TMP_COLOR
. "$TMP_COLOR" # import colors from pywal

# Converts hex colors into rgb joined with comma
# #fff -> 255, 255, 255
hexToRgb() {
    # Remove '#' character from hex color #fff -> fff
    plain=${1#*#}
    printf "%d, %d, %d" 0x${plain:0:2} 0x${plain:2:2} 0x${plain:4:2}
}

prepare() {
    if [ -d $THEME_DIR ]; then
        rm -rf $THEME_DIR
    fi
    
    mkdir $THEME_DIR

}


background=$(hexToRgb $color6)
foreground=$(hexToRgb $foreground)
accent=$(hexToRgb $color13)
secondary=$(hexToRgb $color14)

generate() {
    # Theme template
    cat > "$THEME_DIR/manifest.json" << EOF
    {
      "manifest_version": 3,
      "version": "1.0",
      "name": "$THEME_NAME Theme",
      "theme": {
        "colors": {
          "frame": [$background],
          "frame_inactive": [$secondary],
          "toolbar": [$background],
          "ntp_text": [$foreground],
          "ntp_link": [$accent],
          "ntp_section": [$secondary],
          "button_background": [$background],
          "toolbar_button_icon": [$foreground],
          "toolbar_text": [$foreground],
          "omnibox_background": [$background],
          "omnibox_text": [$foreground]
        },
        "properties": {
          "ntp_background_alignment": "bottom"
        }
      }
    }
EOF
}

prepare
generate
echo "Pywal Chrome theme generated at $THEME_DIR"
