# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# so nvim doesn't get weird colors inside tmux
export TERM="screen-256color"

# Including a NPM dir to install/run packages from
NPM_PACKAGES="${HOME}/.npm-packages"

export PATH="$PATH:$NPM_PACKAGES/bin:$HOME/.local/share/gem/ruby/3.0.0/bin"

# Preserve MANPATH if you already defined it somewhere in your config.
# Otherwise, fall back to `manpath` so we can inherit from `/etc/manpath`.
export MANPATH="${MANPATH-$(manpath)}:$NPM_PACKAGES/share/man"

# Outras variáveis
export BROWSER="/usr/bin/firefox"

# Inicializando o rbenv
eval "$(rbenv init - zsh)"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="cloud"

ZSH_DISABLE_COMPFIX="true"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# Caution: this setting can cause issues with multiline prompts (zsh 5.7.1 and newer seem to work)
# See https://github.com/ohmyzsh/ohmyzsh/issues/5765
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# config
alias x="exit"
alias srz="source ~/.zshrc"
alias zrc="nvim ~/.zshrc"
alias vrc="nvim ~/.config/nvim/init.vim"
alias trc="nvim ~/.tmux.conf"
alias irc="nvim ~/.i3/config"
alias cdot="cd ~/rpo/dotfiles/"
alias edot="cd ~/rpo/dotfiles/; nvim; cd $OLDPWD"
alias ubin="ln -s $HOME/rpo/dotfiles/bin/* $HOME/.local/bin 2> /dev/null"
alias emon="nvim ~/rpo/pessoais/moneylog/txt/conta-bb.txt"

# dev
alias serve="browser-sync start -s -f . --no-notify --host $(hostname -i) --port 1320 --no-open"

# programas
alias mm="matterhorn"

alias kpc="keypass.sh copy"
alias kpu="keypass.sh update"

alias mnd="monitor.sh -d"
alias mnp="monitor.sh -p"
alias mng="monitor.sh -g"

alias sbr="pkexec brillo -S"
alias v="nvim"

## Project aliases
# primeira letra: b = build, c = cd, d = docker, s = start, t = test, u = update, v = vpn
# segunda/terceira letra: projeto
# quarta letra: repositório/ambiente

# classdojo
alias ccdx="cd ~/rpo/wvn/classdojo/external-site"
alias ccda="cd ~/rpo/wvn/classdojo/api"
alias ccdm="cd ~/rpo/wvn/classdojo/web-monorepo"
alias bcdb="nvm use; GATSBY_API_URL=https://api.classdojo.com make build-blog"
alias bcddb="nvm use; GATSBY_API_URL=https://api.classdojo.com make build-devBlog"
alias bcdw="nvm use; GATSBY_API_URL=https://api.classdojo.com make build-www"
alias bcdi="nvm use; GATSBY_API_URL=https://api.classdojo.com make build-ideas"
alias scdb="nvm use; GATSBY_API_URL=https://api.classdojo.com make start-blog"
alias scddb="nvm use; GATSBY_API_URL=https://api.classdojo.com make start-devBlog"
alias scdw="nvm use; make start-prod-api"
alias scdi="nvm use; GATSBY_API_URL=https://api.classdojo.com make start-ideas"
alias scda="nvm use; make start"
alias scdt="cd teach; make run"
alias scdh="cd home; make run"
alias scds="cd student; make run"
alias scdad="cd admin; make run"
alias scdn="cd web; make run-style-guide"
alias ucd="ggl;nvm use;yarn --pure-lockfile"
alias ucdw="ggl;nvm use;yarn --pure-lockfile; npx gatsby clean; node merge-strings-from-directus.js"
alias ucdm="ggl;nvm use;make install-all-dependencies"
alias vcdu="sudo tailscale up"
alias vcdd="sudo tailscale down"
# wolven
alias cnxw="cd ~/rpo/wvn/wolven"
alias snxw="yarn start"
alias unxw="ggl; nvm use; yarn"
# ge
alias cger="cd ~/rpo/wvn/ge/rpv-configurator"
alias uger="npm ci"
alias sger="npm run start"
alias sgers="npm run storybook"
#screensteps
alias cssh="cd ~/rpo/wvn/screensteps/hubspot"
alias csse="cd ~/rpo/wvn/screensteps/screensteps-ember"
alias cssl="cd ~/rpo/wvn/screensteps/screensteps-live"
# weg
alias cwcn="cd ~/rpo/wvn/weg/ui-components/"
alias cwtv="cd ~/rpo/wvn/weg/basic-treeview/"
alias cwst="cd ~/rpo/wvn/weg/studio-app/"
alias swcn="npm -w @orchestra/ui-react-shadcn run storybook"
alias swtv="nvm use 18;npm run dev"
alias swst="npm run dev"
alias bwcn="npm -w @orchestra/ui-react-shadcn run build"
alias bwcns="npm -w @orchestra/ui-react-shadcn run build-storybook"
alias bwcnt="npm -w @orchestra/ui-react-shadcn run build:tw-dev"
alias bwcnc="npm run -w @orchestra/react-lib rollup"
alias bwtv="npm run build"
alias twcne="npm run cypress:open"
alias twcnc="npm run cypress:open-ct"
alias dwss='docker container run -it -p 8888:8888 -e "JAVA_OPTS=--enable-preview"  -e SPRING_PROFILES_ACTIVE=test --rm registry.weg.net/orchestra/studio-api:latest'
alias dwsm='docker container run -it -p 9999:8888 --add-host=host.docker.internal:host-gateway -e SPRING_PROFILES_ACTIVE=local -e "JAVA_OPTS=--enable-preview" -e spring.data.mongodb.port=27017 -e spring.data.mongodb.host=host.docker.internal -e spring.data.mongodb.database=model -e spring.data.mongodb.username=mongo -e spring.data.mongodb.password=mongo --rm registry.weg.net/orchestra/model-api:latest'
alias dwsc='docker container run -it -p 10000:8888 --add-host=host.docker.internal:host-gateway -e SPRING_PROFILES_ACTIVE=local -e model.api.url=http://localhost:9999 -e "JAVA_OPTS=--enable-preview" -e spring.data.mongodb.port=27017 --rm registry.weg.net/orchestra/constraint-api:latest'
alias dwsa='docker container run -it -p 11000:8888 --add-host=host.docker.internal:host-gateway -e SPRING_PROFILES_ACTIVE=local -e "JAVA_OPTS=--enable-preview" -e spring.data.mongodb.port=27017 --rm registry.weg.net/orchestra/application-api:latest'

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# Instalacao das Funcoes ZZ (www.funcoeszz.net)
# script
export ZZPATH=/home/lucas/rpo/dotfile/bin/funcoeszz
# pasta zz/
export ZZDIR=
source /home/lucas/.zzzshrc
