#!/bin/sh

# Prompt.sh, uses dmenu to pick or confirm actions

# Takes 2 arguments
#   * A message that describes the task
#   * The options available

message="$1"
options="$2"

echo -e "$options" | dmenu -i -p "$message" -c -h 32 -l 14 
