#!/bin/bash

# dmenu-i3exit.sh: usa dmenu pra passar um comando pro dmenu-i3exit

actions=$(echo "lock suspend reboot shutdown" | tr " " \\n)

action=$(prompt.sh "i3exit" "$actions")

i3exit "$action"
