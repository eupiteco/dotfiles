#!/bin/sh

sudo apt update
sudo apt list --upgradable

read -p "Gostaria de atualizar o sistema? [S/n]" resposta

[ -z $resposta ] && resposta="s"

if [ $resposta = "s" ]
then
	sudo apt upgrade -y
	read "Pacotes atualizados (:" fim
else
	exit
fi

exit
