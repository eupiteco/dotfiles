#!/bin/bash

ENTRADA="/tmp/lista.txt"

#000000
#212121
#373737
#444444
#727272
#757575
#909090
#BDBDBD
#E0E0E0
#EEEEEE
#F5F5F5
#FFFFFF

while read LINHA
do
	ARQUIVO=$(echo $LINHA | rev | cut -d '/' -f 1 | rev)
	TMP="/tmp/$ARQUIVO.tmp"
	cat "$LINHA" | /tmp/sed-sinzas.sed > $TMP
	echo "> $TMP"
	cat "$TMP" > "$LINHA"
done < $ENTRADA
