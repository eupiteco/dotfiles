#!/bin/bash

search_string="$1"
json_file="$2"

jq -r --arg search_string "$search_string" '
  def search_paths($search_string):
    if type == "object" then
      to_entries | map(.key as $k | (.value | search_paths($search_string)[] | [$k] + .))
    elif type == "array" then
      to_entries | map(.key as $k | (.value | search_paths($search_string)[] | [$k] + .))
      + [[.[] | select(.key == "[]" and .value | search_paths($search_string)[] | [$k] + .)] | flatten]
    elif type == "string" and contains($search_string) then
      [""]
    else
      empty
    end;

  search_paths($search_string) | map(join("."))[]
' "$json_file"
