#!/bin/sh

i3-msg 'exec --no-startup-id telegram-desktop'
i3-msg 'exec --no-startup-id element-desktop'
i3-msg 'exec --no-startup-id discord'
i3-msg 'exec --no-startup-id thunderbird'
i3-msg 'exec --no-startup-id rssguard'
i3-msg 'workspace 5chat'
