#!/bin/sh

if [ "$1" = "-t" ]
then
	SESSION_NAME="$1"
else 
	exit 1
fi

# TMUX helpers
_attach() {
tmux attach -t $SESSION_NAME
}
_open() {
tmux has-session -t $SESSION_NAME 2>/dev/null
if [ $? != 0 ]
then
	tmux new -s $SESSION_NAME -d
else
	_attach
fi

}
_rename() {
tmux rename-window -t $SESSION_NAME "$1"
}
_type() {
tmux send-keys -t $SESSION_NAME "$1" C-m
}
_hSplit() {
tmux split-window -v -t $SESSION_NAME
}
_vSplit() {
tmux split-window -h -t $SESSION_NAME
}
_newWindow() {
tmux new-window -t $SESSION_NAME
}


