#!/bin/sh

# dmenu-goto-window.sh, uses dmenu to focus a window from a list generated
# by wmctrl

window_list=$(wmctrl -l | grep -v "\-1\|urxvt" | cut -f 5- -d " " | cut -c -120)
window=$(prompt.sh "Go to" "$window_list")

wmctrl -a "$window"
