function calculaTamanhoModulo (modulos) {
	console.log(`${modulos} módulos`);
	for (n = 2; n <= modulos/2+1; n++) {
		if ((modulos - n + 1) % n === 0) {
			console.log({
				modulos: n,
				tamanho: (modulos - n + 1) / n,
			});
		}
	}
	console.log(`---------------`);
}

calculaTamanhoModulo(35);
calculaTamanhoModulo(36);
calculaTamanhoModulo(37);
calculaTamanhoModulo(38);
calculaTamanhoModulo(39);
