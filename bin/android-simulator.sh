#!/bin/zsh

if [ "$1" = "--kvm-permission" ]
then
	echo "Setting permissions for the graphical acceleration"
	sudo chown eupiteco:eupiteco /dev/kvm
else
	echo > /dev/null
fi
echo "Running Android Virtual Device"
emulator -avd dev-test
