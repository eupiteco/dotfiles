#!/bin/bash

# Melhor aprender python pra fazer isso aqui, ta doido

#VARS
TEXTOAJUDA="
Calc.sh - um exprimento computacional bem doido
Faz todas operações matemáticas (+, -, *, /, %) usando apenas a soma.

USO: calc.sh [opções] N1 OPERAÇÃO N2

N1 e N2:
  Números inteiros positivos

OPERAÇÃO:
  +: Soma
  -: Subtração
  *: Multiplicação
  /: Divisão
  %: Resto

OPÇÕES:
  -h | --help\tMostra este texto e sai
  -v | --verboso\tMostra todos os passos da operação quando não for uma soma
"

#if [ "$1" -a "$1" = "-v" -o "$1" = "--verboso" ]
#then
	#verboso=1 
	#shift
#else
		##verboso=0
#fi

FATOR1="$1"
FATOR2="$3"
OPERADOR="$2"
[ -n $1 ] && OPERACAO="$@"

#FUNC
_soma() {
	RESULTADO=(( $1 + $2 ))
	OUT="$RESULTADO"
	#[ $verboso ] && OUT="RESULTADO: $OUT"
	echo "RESULTADO: $OUT"
}

_subtrai() {
	#NEGATIVO=$(expr -1 * $2)
	#RESULTADO=$(expr $1 + $NEGATIVO)
	RESULTADO=(( $1 - $2 ))
	OUT="$RESULTADO"
	#echo "INVERSÃO: $2 -> $NEGATIVO"
	#echo "SOMA: $1 + $NEGATIVO"
	echo "RESULTADO: $OUT"
}

_multiplica() {
	if [ $1 -lt $2 ] 
	then
		MAIOR=$2
		MENOR=$1 
	else
		MAIOR=$1
		MENOR=$2 
	fi
	VOLTA=0
	TOTAL=0
	while [ $VOLTA -lt $MENOR ]; do
		VOLTA=(( $VOLTA + 1 ))
		TOTAL=(( $TOTAL + $MAIOR ))
	done
	OUT=$TOTAL
	echo "RESULTADO: $OUT"
}

_divide() {
	echo "nada"
}

_resto() {
	echo "nada"
}

_help() {
	echo "$TEXTOAJUDA" | expand -t 9
}

#PROGRAMA

if [ "$1" = "-h" -o "$1" = "--help" ]
then
	_help
	exit
fi

echo "OPERAÇÃO: $OPERACAO"
case $OPERADOR in
	"+")
		_soma $FATOR1 $FATOR2
		;;
	"-")
		_subtrai $FATOR1 $FATOR2
		;;
	"*")
		_multiplica $FATOR1 $FATOR2
		;;
	"/")
		_divide $FATOR1 $FATOR2
		;;
	"%")
		_resto $FATOR1 $FATOR2
		;;
	*)
		echo "Operador não reconhecido, tente calc.sh --help"
		;;
esac
