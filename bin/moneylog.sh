# Ações do moneylog, edita txt e abre no navegador

view="firefox --private ~/moneylog/moneylog.html"

prompt() {
	options=$(ls moneylog/txt | tr \\t \\n | cut -f 1 -d .)
	options="$options\nview"
	selected=$(prompt.sh "Moneylog" "$options")
	if [ $selected = "view" ]
	then
		$view
	else
		open $selected
	fi
}

edit() {
	vim ~/moneylog/txt/"$1".txt
}

open() {
	rxvt-unicode -e vim ~/moneylog/txt/"$1".txt
}


case "$1" in
	principal)
		edit principal
		;;
	poupanca)
		edit poupanca
		;;
	vr)
		edit vr
		;;
	view)
		$view
		;;
	*)
		prompt
		;;
esac

