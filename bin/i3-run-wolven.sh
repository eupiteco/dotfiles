#!/bin/sh

i3-msg 'exec --no-startup-id firefox --new-window app.tmetric.com'
i3-msg 'exec --no-startup-id firefox --new-window app.asana.com'
i3-msg 'exec --no-startup-id kitty --title "Dev IDE"'
i3-msg 'exec --no-startup-id chromium'
i3-msg 'exec --no-startup-id thunderbird'
i3-msg 'exec --no-startup-id mattermost-desktop'
i3-msg 'exec --no-startup-id slack'
i3-msg 'exec --no-startup-id chromium --new-window https://open.spotify.com'
i3-msg 'exec --no-startup-id kitty --title "Notas" -e nvim ~/txt/wolven.md'
i3-msg 'exec --no-startup-id keepassxc'

sleep 5

i3-msg 'workspace 1www'
i3-msg '[title="Nova guia"] move workspace 3test'
i3-msg '[title="Spotify"] move workspace 6media'
i3-msg '[title="Asana|asana"] move workspace 7todo'
i3-msg '[title="TMetric|tmetric"] move workspace 8dash'
i3-msg 'exec --no-startup-id firefox --new-window'
i3-msg 'workspace 8dash'
