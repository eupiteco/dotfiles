#!/bin/sh

# Capture a area of the screen with imagemagikc's import tool
# Captura uma área da tela com a ferramenta import do pacote imagemagick

SS_FOLDER="/tmp/"
SS_NAME=$(date +%d%h%y_%H0%M%S)
SS_EXTENSION=".png"

import -delay 1000 $SS_FOLDER$SS_NAME$SS_EXTENSION
