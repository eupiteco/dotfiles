#!/bin/sh

# Alterna entre usuários do git

case $1 in
	meu)
		git config --global user.name eupiteco
		git config --global user.email eupiteco@gmail.com
		break
		;;
	wvn)
		git config --global user.name lucas.piteco
		git config --global user.email lucas.piteco@nextt.com.br
		break
		;;
	weg)
		git config --global user.name e-piteco
		git config --global user.email e-piteco@weg.net
		break
		;;
	*) ;;
esac

echo $(git config --global user.name)
echo $(git config --global user.email)
echo
echo "Disponíveis: meu wvn weg"
