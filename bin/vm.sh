#!/bin/bash

USER_HOME="$HOME"
ACTION=$1

function _setVm {
	case $1 in
		redweek)
			VM="Redweek"
			PORT=2224
			MNT="$USER_HOME/nextt/vm/redweek/"
			REMOTE="rw@$1.local:/home/rw/repo"
			;;
		classdojo)
			VM="Classdojo"
			PORT=2223
			MNT="$USER_HOME/nextt/vm/classdojo/"
			REMOTE="piteco@$1.local:/home/piteco/repos"
			;;
		screensteps)
			VM="Screensteps"
			PORT=2222
			MNT="$USER_HOME/nextt/vm/screensteps/"
			REMOTE="piteco@$1.local:/home/piteco/repos"
			;;
	esac
}
function _unmount {
  sudo umount $MNT
}

function _mount {
  sudo sshfs -o allow_other,auto_cache,reconnect,default_permissions,no_readahead,IdentityFile=$USER_HOME/.ssh/id_rsa $REMOTE -p $PORT $MNT
}

function _stopvm {
  VBoxManage controlvm $VM savestate
}

function _startvm {
  VBoxManage startvm $VM --type headless
}

function _usage {
    echo "Usage: startvm.sh <action> <vm_name> [-mount <mount_point>]]"
    echo "Available actions: start | stop | status | remount | unmount"
    echo "Available vms: " && VBoxManage list vms
}

function _debug {
  echo "Variable data"
  echo $1
  echo $VM
  echo $USER_HOME
  echo $MNT
  echo $REMOTE
}

if [ "$2" != "" ]
then
	_setVm $2
else
	_usage
	exit
fi

# while [ "$2" != "" ]; do
# 		case $2 in
# 				-mount )          shift
# 													MNT=$2
# 													;;
# 				-h | --help )     _usage
# 													exit
# 													;;
# 		esac
# 		shift
# done

case $ACTION in
  stop)
    echo "Stopping VM: $VM"
    _unmount
    _stopvm
  ;;
  start)
    echo "Starting VM: $VM"
    _startvm
    _unmount
    _mount
  ;;
  remount)
    echo "Mounting Remote Folder"
    _unmount
    _mount
  ;;
  unmount)
    echo "Unmounting Remote Folder"
    _unmount
  ;;
  status)
    echo "List of Running VMs:" && VBoxManage list runningvms
  ;;
  *)
    _usage
    _debug
  ;;
esac
exit 0
# eof
