#!/bin/sh

# dmenu-group-launch.sh: uses dmenu to run i3 scripts that launches specific
# application groups. Scripts mus be in $bin_dir and start with the $prefix 
# prefix.

bin_dir="$HOME/.local/bin/"
prefix="i3-run-"

launchers=$(ls "$bin_dir" | grep "$prefix" | cut -f 3 -d "-" | cut -f 1 -d ".")
selectedLauncher=$(prompt.sh "Abrir grupo:" "$launchers")

"$prefix$selectedLauncher.sh"
