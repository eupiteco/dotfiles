
#!/bin/sh

i3-msg 'exec --no-startup-id qutebrowser https://gitlab.com/eupiteco/pessoal/-/boards/4562433'
i3-msg 'exec --no-startup-id urxvt -title "Notas" -e nvim ~/txt/'

sleep 5

i3-msg '[title="GitLab"] move workspace 7todo'
i3-msg 'workspace 7todo'
