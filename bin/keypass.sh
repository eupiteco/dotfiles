LIST_FILE="/home/lu/pass.fzf"
PASS_FILE="/home/lu/passwd.kdbx"

_updateList() {
	keepassxc-cli ls -R "$PASS_FILE" | grep -v \/$ | sed 's/^ \+//g' > "$LIST_FILE"
}

_getPassword() {
	keepassxc-cli show "$PASS_FILE" "$(fzf < $LIST_FILE)"	
}

case $1 in
	copy) _updateList ;;
	update) _getPassword ;;
	*) echo "Argumento inválido, use copy ou update" ;;
esac
