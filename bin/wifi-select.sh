#!/bin/bash

# Seletor de wifi usando dmenu, nmcli

redes=$( nmcli dev wifi | sed 's/^[ *]*//;1d' | cut -f 1 -d ' ' | head -n 5)

redeSelecionada=$(prompt.sh "Conectar em:" "$redes")

echo -n "Senha para $redeSelecionada: "
read -s senha

echo "Connectando..."
nmcli dev wifi connect $redeSelecionada password $senha
