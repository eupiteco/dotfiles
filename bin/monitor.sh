#!/bin/sh

# Auxiliar para usar o xrandr com cabo HDM1
# Uso:
# 	-d dual
# 	-g grande
# 	-p pequeno

SMALL_RES="1440x810" # Não funciona, aparece na listagem do xrandr mas quando seta diz que não existe
BIG_RES="1920x1080"

prompt_monitor() {
# Setting monitors using dmenu
# Uso:
# 	Pergunta se dual ou single
# 	SE single pergunta qual usar >> xrandr
# 	SE dual pergunta qual o primário
# 	SE mais de dois monitores conectados, pergunta qual o secundário

	modes="single\ndual"
	monitors=$(xrandr | grep " connected" | cut -f 1 -d " ")
	selectedMode=$(prompt.sh "Selecionar modo:" "$modes")
	case $selectedMode in
		single)
			selectedMonitor=$(prompt.sh "Selecione o monitor:" "$monitors")
			otherMonitors=$(echo "$monitors" | grep -v $selectedMonitor)
			xrandrParams="--output $selectedMonitor --mode $BIG_RES --primary"
			for monitor in $otherMonitors
			do
				xrandrParams="$xrandrParams --output $monitor --off"
			done
			;;
		dual)
			directions="above\nright\nbelow\nleft"
			monitorCount=$(echo $monitors | cat -n | cut -f 1 | tail -n 1 | tr -d " ")
			primaryMonitor=$(prompt.sh "Selecione o monitor primário:" "$monitors")
			if [ $monitorCount -gt 2 ]
			then
				monitors=$(echo $monitors | grep -v $primaryMonitor)
				secondaryMonitor=$(prompt.sh "Selecione o monitor secundário:" "$monitors")
			else
				secondaryMonitor=$(echo "$monitors" | grep -v $primaryMonitor)
			fi
			selectedDirection=$(prompt.sh "Selecione a orientação do monitor secundário:" "$directions")
			case $selectedDirection in
				above)
					selectedDirection="--above"
					;;
				right)
					selectedDirection="--right-of"
					;;
				below)
					selectedDirection="--below"
					;;
				left)
					selectedDirection="--left-of"
					;;
			esac
		xrandrParams="--output $primaryMonitor --mode $BIG_RES --primary --output $secondaryMonitor --mode $BIG_RES $selectedDirection $primaryMonitor"
		;;
	esac
}

handle_dual_monitor() {
	active_monitor=$(xrandr | grep primary | cut -f 1 -d " ")
	if [ $active_monitor = "eDP-1-1" ]
	then
		xrandrParams=""
	else
		xrandrParams="--output eDP-1 --mode $BIG_RES --primary"
	fi
	xrandrParams="$xrandrParams --output HDMI-2 --above eDP-1 --mode $BIG_RES"
}

case $1 in
	# -d) handle_dual_monitor ;;
	-d) xrandrParams="--output eDP-1 --left-of HDMI-1-0 --auto --output HDMI-1-0 --auto --primary" ;;
	-g) xrandrParams="--output eDP-1 --off --output HDMI-1-0 --auto --primary" ;;
	-p) xrandrParams="--output HDMI-1-0 --off --output eDP-1 --auto --primary" ;;
	*) prompt_monitor ;;
esac

xrandr $xrandrParams
