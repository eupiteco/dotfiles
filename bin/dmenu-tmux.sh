#!/bin/sh

# dmenu-tmux.sh: uses dmenu to attatch a shell to a specific tmux session

bin_dir="$HOME/.local/bin/"
prefix="i3-run-"

sessions=$( tmux list-sessions | cut -f 1 -d :)
selectedSession=$(prompt.sh "Abrir sessão:" "$sessions")

tmux a -t "$selectedSession"
