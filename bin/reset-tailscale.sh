#!/bin/bash

echo
echo -n "Linking resolv.conf..."
echo
sudo ln -sf /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf
echo "OK"
echo -n "Restarting systemd-resolved..."
sudo systemctl restart systemd-resolved
echo "OK"
echo -n "Restarting network-manager..."
sudo systemctl restart NetworkManager
echo "OK"
echo -n "Restarting tailscale..."
sudo systemctl restart tailscaled 
echo "OK"
