#!/bin/sh

info="\
###########################\n\
\n\
url: fernanda.collective2.com/selector\n\
user: fernanda@collective2.com\n\
pswd: fm691mt7\n\
\n\
###########################"

case "$1" in
	"info")
		echo "$info"
		;;	
	"run")
		ssh c2 -v
		;;
	"watch")
		unison c2 -ui text
		;;
	* )
		echo "Valid options are 'run', 'watch' and 'info'."
		;;
esac
