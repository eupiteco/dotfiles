# coding=UTF-8

import subprocess

from i3pystatus import Status
from i3pystatus.weather import weathercom
from i3pystatus.updates import aptget
from i3pystatus.mail import imap

def read_xresources(prefix):
    props = {}
    x = subprocess.run(['xrdb', '-query'], stdout=subprocess.PIPE)
    lines = x.stdout.decode().split('\n')
    for line in filter(lambda l : l.startswith(prefix), lines):
        prop, _, value = line.partition(':\t')
        props[prop] = value
    return props

xresources = read_xresources('*')

fg = xresources['*.foreground']
bg = xresources['*.background']
db = xresources['*.color0']
dr = xresources['*.color1']
dg = xresources['*.color2']
dy = xresources['*.color3']
du = xresources['*.color4']
dm = xresources['*.color5']
dc = xresources['*.color6']
dw = xresources['*.color7']
lb = xresources['*.color8']
lr = xresources['*.color9']
lg = xresources['*.color10']
ly = xresources['*.color11']
lu = xresources['*.color12']
lm = xresources['*.color13']
lc = xresources['*.color14']
lw = xresources['*.color15']

# bg = '#d8d8d8'
# fg = '#222d31'
# db = '#222d31'
# dr = '#ab4642'
# dg = '#7e807e'
# dy = '#f7ca88'
# du = '#7cafc2'
# dm = '#ba8baf'
# dc = '#1abb9b'
# dw = '#d8d8d8'
# lb = '#585858'
# lr = '#ab4642'
# lg = '#8d8f8d'
# ly = '#f7ca88'
# lu = '#7cafc2'
# lm = '#ba8baf'
# lc = '#1abb9b'
# lw = '#f8f8f8'

status = Status()

status.register(
    "text",
    text="",
)

# status.register(
#     "redshift",
#     color=lr,
#     format="💡 {inhibit}"
# )

# status.register(
#     "network",
#     interface="wlp2s0",
#     color_up=lw,
#     format_up="📶",
#     format_down="",
# )

status.register(
    "pulseaudio",
    color_unmuted=fg,
    color_muted=dr,
    format="♪{volume}",
)

# status.register(
#     "backlight",
#     backlight="intel_backlight",
#     color=lw,
#     format="🔅{percentage}"
# )

status.register(
    "text",
    text="|",
    color=fg,
)

# status.register(
#     "moon",
#     format="{moonicon}",
#     color={
#         'New Moon': dw,
#         'Last Quarter': dw,
#         'Full Moon': dw,
#         'Waning Crescent': dw,
#         'Waxing Crescent': dw,
#         'Waxing Gibbous': dw,
#         'Waning Gibbous': dw,
#         'First Quarter': dw,
#     },
#     moonicon={
#         'New Moon': '🌕',
#         'Waxing Crescent': '🌔',
#         'First Quarter': '🌓',
#         'Waning Crescent': '🌖',
#         'Full Moon': '🌑',
#         'Waning Gibbous': '🌘',
#         'Last Quarter': '🌗',
#         'Waxing Gibbous': '🌒',
#         }
# )

# status.register(
#     "weather",
#     backend=weathercom.Weathercom(
#         location_code='af83edc23cc660c4cf43691a22012da6c5732052edb4ea4de3fe3714a5e2bd98',
#     ),
#     color=lw,
#     format="{icon} {current_temp}{temp_unit} {wind_speed}{wind_unit} {wind_direction}",
# )

status.register(
    "clock",
    color=fg,
    format="%a %-d %b %H:%M",
)

status.register(
    "text",
    text="|",
    color=fg,
)

status.register(
    "text",
    text="|",
    color=fg,
)

status.register(
    "battery",
    format="{status} {remaining:%hh:%Mm}",
    alert=True,
    alert_percentage=15,
    color=dy,
    charging_color=dg,
    critical_color=dr,
    no_text_full=True,
    status={
        "DIS": "↓",
        "CHR": "↑",
        "FULL": "=",
    },
)

status.register(
    "cpu_usage",
    dynamic_color=True,
        start_color=dg,
        end_color=dr,
    format="CPU {usage}%",
)

status.register(
    "mem",
    color=dg,
    warn_color=dy,
    alert_color=dr,
    format="MEM {percent_used_mem}%",
)

status.register(
    "disk",
    color=dg,
    critical_color=dr,
    critical_limit=20,
    path="/",
    round_size=None,
    format="SSD {used}/{total}G",
)


status.register(
    "text",
    text="|",
    color=fg,
)

# status.register(
#     'github',
#     notify_status=True,
#     notify_unread=True,
#     access_token='a6ba199820cbc9b85dd2cd96222c256b473bf6f8',
#     hints={'markup': 'pango'},
#     update_error='<span color="#af0000">!</span>',
#     refresh_icon='<span color="#ff5f00">⟳</span>',
#     unread_marker='<span color="#2288ff">•</span>',
#     status={
#         'good': '✓',
#         'minor': '!',
#         'major': '!!',
#     },
#     colors={
#         'good': dg,
#         'minor': dy,
#         'major': dr,
#     },
#     format="GH {unread}",
# )


# status.register(
#     "updates",
#     format = "⟳ {count}",
#     format_no_updates = "",
#     backends = [aptget.AptGet()],
#     color=lr,
#     interval=259200, # a cada 3 dias
#     on_leftclick="rxvt-unicode -e upgrade.sh"
# )

# status.register(
#     "scratchpad",
#     color=lw,
#     always_show=False,
#     color_urgent=dr,
# )

status.register (
    "pomodoro",
    sound="~/.config/i3pystatus/pomodoro.mp3"
)

status.run()
